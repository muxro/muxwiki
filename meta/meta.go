// Package meta supplies the user with a
package meta

import (
	"os"
	"path"

	"github.com/go-chi/chi/v5"
	"gitlab.com/muxro/muxwiki/manager"
	"gitlab.com/muxro/muxwiki/paste"
	"gitlab.com/muxro/muxwiki/wiki"
)

// Config is the config that is passed to NewMeta
type Config struct {
	Password     string
	DataDir      string
	EnableWiki   bool
	EnableMedia  bool
	EnablePastes bool
}

// Meta is the meta handler, it should be everything that is needed to have muxwiki imported in other projects
// main.go is used just for the standalone application
type Meta struct {
	Conf Config

	manager manager.Manager

	wiki  wiki.Wiki
	media paste.Paste
	paste paste.Paste
}

// NewMeta returns a new Meta instance, encompassing everything
func NewMeta(conf Config) *Meta {
	return &Meta{
		Conf: conf,
	}
}

// Router returns a new router
func (m *Meta) Router() chi.Router {
	r := chi.NewRouter()

	// initialize everything
	m.manager = manager.NewManager(m.Conf.DataDir, 6)

	if m.Conf.EnablePastes {
		os.MkdirAll(path.Join(m.Conf.DataDir, "pastes"), 0777)
		m.paste = paste.NewPaste(m.manager, "pastes", "p")
		r.Mount("/p/", m.paste.Router())
	}

	if m.Conf.EnableMedia {
		os.MkdirAll(path.Join(m.Conf.DataDir, "media"), 0777)
		m.media = paste.NewPaste(m.manager, "media", "m")
		r.Mount("/m/", m.media.Router())
	}

	if m.Conf.EnableWiki {
		os.MkdirAll(path.Join(m.Conf.DataDir, "wiki"), 0777)
		m.wiki = wiki.NewWiki(m.manager, wiki.ArticleTemplate, wiki.EditorTemplate)
		// landing page
		r.Get("/", m.wiki.RenderMarkdown)
		r.Get("/edit", m.wiki.WikiEditor)

		r.Mount("/w/", m.wiki.Router())
	}

	r.Post("/", m.uploadHandler)
	return r
}
