package meta

import (
	"io"
	"mime"
	"net/http"
	"path/filepath"
	"strings"
)

var (
	mediaPrefixes = []string{
		"audio",
		"video",
		//"application",
		"font",
		"image",
	}
)

func (m *Meta) passwordChecker(r *http.Request) bool {
	return m.Conf.Password == "" || strings.TrimSpace(r.FormValue("pwd")) == m.Conf.Password
}

func (m *Meta) uploadHandler(w http.ResponseWriter, r *http.Request) {
	r.ParseMultipartForm(10 * 1024 * 1024)

	if (m.Conf.EnableMedia || m.Conf.EnablePastes) && r.MultipartForm != nil && len(r.MultipartForm.File) == 1 && len(r.MultipartForm.File["f:1"]) == 1 {
		if !m.passwordChecker(r) {
			http.Error(w, "You must specify a valid password", 400)
			return
		}
		var isMedia bool
		// File upload, deduce file type
		fileData, err := r.MultipartForm.File["f:1"][0].Open()
		fileName := r.MultipartForm.File["f:1"][0].Filename
		if err != nil {
			http.Error(w, "could not open form file", 500)
			return
		}

		fileType, err := sniffType(fileData, fileName)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}

		if fileType != "" && fileType != "application/octet-stream" { // try to find an actual content type
			for _, mediaPrefix := range mediaPrefixes {
				if strings.HasPrefix(fileType, mediaPrefix) {
					isMedia = true
					break
				}
			}
		}

		// TODO: this fails a lot (it usually puts media in pastes), but it's not a big problem (I hope)
		if isMedia && m.Conf.EnableMedia {
			m.media.HandlePasteUpload(fileData, fileName, w, r)
		} else if m.Conf.EnablePastes {
			m.paste.HandlePasteUpload(fileData, fileName, w, r)
		}

	} else if m.Conf.EnablePastes && r.MultipartForm != nil && len(r.MultipartForm.Value) == 1 {
		if !m.passwordChecker(r) {
			http.Error(w, "You must specify a valid password", 400)
			return
		}
		// no name paste upload handling
		if val, ok := r.MultipartForm.Value["f:1"]; ok && len(val) > 0 {
			m.paste.HandlePasteUpload(strings.NewReader(val[0]), "untitled", w, r)
		}
	} else if m.Conf.EnableWiki {
		// Wiki update
		m.wiki.HandleEdit(w, r)
	}
}

// Inspired by net/http's type sniffing in serveContent
func sniffType(content io.ReadSeeker, name string) (string, error) {
	ctype := mime.TypeByExtension(filepath.Ext(name))
	if ctype == "" {
		var buf [512]byte
		n, _ := io.ReadFull(content, buf[:])
		ctype = http.DetectContentType(buf[:n])
		_, err := content.Seek(0, io.SeekStart)
		if err != nil {
			return "", err
		}
	}
	return ctype, nil
}
