package wiki

// ArticleTemplate is the default template for articles
var ArticleTemplate = `
{{define "header"}}
{{if not .Children}}
<details>
    <summary>
        <a href="#{{.ID}}">{{.Name}}</a>
    </summary>
</details>
{{else}}
<details {{if le .Depth 2}}open{{end}}>
    <summary>
        <a href="#{{.ID}}">{{.Name}}</a>
    </summary>
    {{if .Children}}
    {{range .Children}}
    {{template "header" .}}
    {{end}}
    {{end}}
</details>
{{end}}
{{end}}

<html>

<head>
    <style>
        summary::-webkit-details-marker {
            color: #00ACF3;
            font-size: 125%;
            margin-right: 2px;
        }

        summary:focus {
            outline-style: none;
        }

        article>details>summary {
            font-size: 16px;
            margin-top: 16px;
        }

        details>a {
            margin-left: 12px;
        }

        details details {
            margin-left: 24px;
        }

        details details summary {
            font-size: 12px;
        }


        :root {
            --dark-font-color: #CCC;
            --dark-background-color: #262626;
            --light-font-color: black;
            --light-background-color: white;
        }

        .language-config {
            display: none;
        }

        html {
            -webkit-text-size-adjust: 100%;
            padding-bottom: 4em;
        }

        body {
            font-family: sans-serif;
            line-height: 1.5;
            padding: 0 2%;
            margin: auto;
        }

        @media (prefers-color-scheme: dark) {
            body {
                background-color: var(--dark-background-color);
                color: var(--dark-font-color);
            }
        }

        @media (prefers-color-scheme: light no-preference) {
            body {
                background-color: var(--light-background-color);
                color: var(--light-font-color);
            }
		}
		
        body.force-dark {
            background-color: var(--dark-background-color);
            color: var(--dark-font-color);
		}
		
        body.force-light {
            background-color: var(--light-background-color);
            color: var(--light-font-color);
        }

        .text-input {
            width: 96%;
            display: block;
            padding: .5em 1%;
            margin-bottom: 1.5em;
            font-size: 1em;
        }

        fieldset {
            margin-bottom: 1.5em;
        }

        textarea {
            height: 10em;
        }

        dt {
            font-weight: bold;
        }

        .important {
            color: red;
        }

        .footer {
            text-align: right;
        }

        .md {
            flex-grow: 3;
        }

        .flexer {
            display: flex;
        }

        .index {
            width: 300px;
            flex-grow: 1;
            border-width: 2px;
            border-color: grey;
            margin: 5px;
        }
    </style>
    <title>{{ .Title }}</title>
</head>
{{ if .Dark }}

<body class="force-dark">
    {{ else if .Light }}

    <body class="force-light">
        {{ else }}

        <body>
            {{ end }}

            <div class="flexer">
                {{if .Index}}
                <div class="index">
                    <h2>Contents:</h2>
                    {{range .Index}}
                    <article id="index">
                        {{template "header" .}}
                    </article>
                    {{end}}
                </div>
                {{end}}
                <div class="md">
                    <h1>{{.MetaData.title}}</h1>
                    {{.Markdown}}
                </div>

                <footer>
                    <h6>
                        Last edited by {{.MetaData.lasteditor}} at {{.MetaData.lastedit}}
                    </h6>
                </footer>
            </div>
        </body>

</html>
`

// EditorTemplate is the default template for the wiki editor
var EditorTemplate = `
<html>

<head>
    <title>
        Editor
    </title>
</head>
<style>
    #editor {
        width: 95%;
        height: 80vh;
    }
</style>

<body>
    <form spellcheck="false" autocapitalize="off" autocomplete="off" action="/" id="editForm" method="POST">
        <input type="hidden" name="page" value="{{.Article}}"><br>
        <input type="text" name="title" placeholder="Title" value='{{.Title}}'><br>
        <input type="text" name="editor" placeholder="Who is editing?"><br>
        <input type="submit" value="Submit">
    </form>
    <!--
        <textarea spellcheck="false" form="editForm" name="data" required rows="35" cols="100">.OriginalMarkdown</textarea>
		-->
    <textarea form="editForm" name="data" style="display: none;"></textarea>
    <div id="editor">{{.OriginalMarkdown}}</div>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.7/ace.js" type="text/javascript"
        charset="utf-8"></script>
    <script>
        var editor = ace.edit("editor");
        editor.session.setUseWrapMode(true);
        editor.setHighlightActiveLine(true);
        editor.setTheme("ace/theme/monokai");
        editor.session.setMode("ace/mode/markdown");
        var textarea = $('textarea[name="data"]');
        editor.session.on('change', function (delta) {
            textarea.val(editor.getSession().getValue());
        });
    </script>

</body>

</html>
`
