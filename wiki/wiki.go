package wiki

import (
	"bytes"
	"fmt"
	"html/template"
	"net/http"
	"strings"
	"time"

	hhtml "github.com/alecthomas/chroma/formatters/html"
	"github.com/go-chi/chi/v5"
	"github.com/yuin/goldmark"
	highlighting "github.com/yuin/goldmark-highlighting"
	meta "github.com/yuin/goldmark-meta"
	"github.com/yuin/goldmark/ast"
	"github.com/yuin/goldmark/extension"
	"github.com/yuin/goldmark/parser"
	"github.com/yuin/goldmark/renderer/html"
	"github.com/yuin/goldmark/text"
	"gitlab.com/muxro/muxwiki/manager"
	"gitlab.com/muxro/muxwiki/paste"
	"gopkg.in/yaml.v2"
)

const (
	mode         = "wiki"
	dataStartSep = "---"
	dataEndSep   = "---"
)

// Wiki is an instance for the wiki element
type Wiki struct {
	md goldmark.Markdown

	manager         manager.Manager
	articleTemplate *template.Template
	editorTemplate  *template.Template
}

// ArticleMeta specifies all stuff
type ArticleMeta struct {
	Title      string `yml:"title"`
	LastEdit   string `yml:"lastEditedAt"`
	LastEditor string `yml:"lastEditedBy"`
}

// HeadLevel specifies data about what to be put in the index
type HeadLevel struct {
	Name     string
	ID       string
	Children []HeadLevel
	Depth    int
}

// WikiEditor renders the editor view
func (wiki *Wiki) WikiEditor(w http.ResponseWriter, r *http.Request) {
	var data ArticleMeta
	file := wiki.ReadMarkdown(chi.URLParam(r, "article"))

	actualMD, err := wiki.ExtractMeta(&data, file)
	if err != nil {
		http.Error(w, "Error: "+err.Error(), 500)
		return
	}

	if data.Title == "" {
		data.Title = chi.URLParam(r, "article")
	}

	err = wiki.editorTemplate.Execute(w, struct {
		Article          string
		OriginalMarkdown string
		Title            string
	}{
		Article:          chi.URLParam(r, "article"),
		OriginalMarkdown: string(actualMD),
		Title:            data.Title,
	})
	if err != nil {
		http.Error(w, err.Error(), 500)
	}
}

// HandleEdit handles an edit
func (wiki *Wiki) HandleEdit(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()

	articleMD := r.PostFormValue("data")
	editor := r.PostFormValue("editor")
	page := r.PostFormValue("page")
	title := r.PostFormValue("title")
	stringedMeta, err := yaml.Marshal(ArticleMeta{LastEditor: editor, LastEdit: time.Now().UTC().Format(time.ANSIC), Title: title})
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	newOutput := fmt.Sprintf("---\n%s---\n%s", string(stringedMeta), articleMD)

	if page == "" {
		page = "index"
	}
	page = strings.TrimSuffix(page, ".md")

	wiki.manager.SaveDataName(mode, page, strings.NewReader(newOutput))
	if page != "" {
		page = "w/" + page
	}

	fmt.Fprintf(w, "%s/%s", paste.GetHostname(r), page)
}

// GetSource returns the source for an article
func (wiki *Wiki) GetSource(w http.ResponseWriter, r *http.Request) {
	w.Write(wiki.ReadMarkdown(chi.URLParam(r, "article")))
}

// RenderMarkdown renders the markdown of the article
func (wiki *Wiki) RenderMarkdown(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "article")
	file := wiki.ReadMarkdown(name)
	_, isDark := r.URL.Query()["dark"]
	_, isLight := r.URL.Query()["light"]

	var data map[string]interface{}
	var buf bytes.Buffer

	context := parser.NewContext()
	if err := wiki.md.Convert(file, &buf, parser.WithContext(context)); err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	data = meta.Get(context)

	title, ok := data["title"]
	if !ok {
		title = chi.URLParam(r, "article")
	}

	parent := wiki.md.Parser().Parse(text.NewReader(file))

	var indexes []HeadLevel
	walker := func(node ast.Node, entering bool) (ast.WalkStatus, error) {
		if entering == false {
			return 0, nil
		}

		if node.Kind() == ast.KindHeading {
			switch tok := node.(type) {
			case *ast.Heading:
				id, _ := tok.AttributeString("id")
				var fullName string
				if tok.HasChildren() {
					child := tok.FirstChild()
					for i := 0; i < tok.ChildCount(); i++ {
						fullName += string(child.Text(file))
						child = child.NextSibling()
					}
				}
				index := HeadLevel{
					Name:     fullName,
					ID:       string(id.([]uint8)),
					Children: []HeadLevel{},
					Depth:    tok.Level,
				}
				var presentIndex *[]HeadLevel
				presentIndex = &indexes
				for i := tok.Level; i > 1; i-- {
					if len(*presentIndex) == 0 {
						presentIndex = &[]HeadLevel{}
						break
					}
					presentIndex = &((*presentIndex)[len((*presentIndex))-1].Children)
				}
				*presentIndex = append(*presentIndex, index)
			}
		}
		return 0, nil
	}

	err := ast.Walk(parent, walker)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	if err = wiki.articleTemplate.Execute(w, struct {
		Markdown template.HTML
		Dark     bool
		Light    bool
		Title    string
		Index    []HeadLevel
		MetaData map[string]interface{}
	}{
		Markdown: template.HTML(string(buf.Bytes())),
		Dark:     isDark,
		Light:    isLight,
		Title:    title.(string),
		Index:    indexes,
		MetaData: data,
	}); err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
}

// ReadMarkdown reads the markdown file for an article
func (wiki *Wiki) ReadMarkdown(name string) []byte {
	if name == "" {
		name = "index.md"
	}
	if !strings.HasSuffix(name, ".md") {
		name += ".md"
	}
	file, err := wiki.manager.GetData(mode, name)
	if err != nil {
		return []byte("File not found. Try creating it at /w/" + name[:strings.Index(name, ".")] + "/edit")
	}

	return file
}

// ExtractMeta gives the meta information from a byte slice
func (wiki *Wiki) ExtractMeta(v *ArticleMeta, file []byte) ([]byte, error) {
	var data, actualMD []byte
	if strings.HasPrefix(string(file), dataStartSep) {
		endAppearance := strings.Index(string(file)[1:], dataEndSep)
		endAppearance++
		data, actualMD = file[len(dataStartSep):endAppearance], file[endAppearance+len(dataEndSep):]
	} else {
		actualMD = file
	}

	err := yaml.Unmarshal(data, &v)
	if err != nil {
		return nil, err
	}

	return actualMD, nil
}

// Router returns a valid router for the wiki module
func (wiki *Wiki) Router() chi.Router {
	r := chi.NewRouter()
	r.Route("/{article}", func(r chi.Router) {
		r.Get("/source", wiki.GetSource)
		r.Get("/", wiki.RenderMarkdown)
		r.Get("/edit", wiki.WikiEditor)
	})
	return r
}

// NewWiki returns a new Wiki instance
func NewWiki(manager manager.Manager, articleTempl, editorTempl string) Wiki {
	// initialize the markdown renderer
	md := goldmark.New(
		goldmark.WithExtensions(
			extension.Linkify,
			extension.GFM,
			highlighting.NewHighlighting(
				highlighting.WithStyle("monokai"),
				highlighting.WithFormatOptions(
					hhtml.WithLineNumbers(true),
					hhtml.TabWidth(4),
				),
			),
			meta.Meta,
		),
		goldmark.WithParserOptions(
			parser.WithBlockParsers(),
			parser.WithInlineParsers(),
			parser.WithAttribute(),
			parser.WithASTTransformers(),
			parser.WithAutoHeadingID(),
		),
		goldmark.WithRendererOptions(
			html.WithXHTML(),
		),
	)
	articleTemp, err := template.New("article").Parse(articleTempl)
	if err != nil {
		// if the dev didn't provide a valid template, it's his fault
		panic(err)
	}
	editorTemp, err := template.New("editor").Parse(editorTempl)
	if err != nil {
		panic(err)
	}
	return Wiki{
		md:              md,
		manager:         manager,
		articleTemplate: articleTemp,
		editorTemplate:  editorTemp,
	}
}
