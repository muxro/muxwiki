module gitlab.com/muxro/muxwiki

go 1.19

require (
	github.com/alecthomas/chroma v0.10.0
	github.com/go-chi/chi/v5 v5.0.7
	github.com/yuin/goldmark v1.5.2
	github.com/yuin/goldmark-highlighting v0.0.0-20220208100518-594be1970594
	github.com/yuin/goldmark-meta v1.1.0
	gopkg.in/yaml.v2 v2.4.0
)

require github.com/dlclark/regexp2 v1.7.0 // indirect
