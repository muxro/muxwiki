// main.go creates a new Meta instance and webserver, should be used for the standalone binary
package main

import (
	"flag"
	"log"
	"math/rand"
	"net/http"
	"os"
	"path"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/muxro/muxwiki/meta"
)

var (
	// defaults
	defaultPort = "3000"

	port    = flag.String("port", defaultPort, "Specify the port used to listen")
	dataDir = flag.String("dataDir", "", "Specify the current working directory")
)

func overwriteFlag(flag *string, defVal string, envTerm string) {
	val, exists := os.LookupEnv(envTerm)
	if *flag == defVal && exists {
		*flag = val
	}
}

func main() {
	rand.Seed(time.Now().UnixNano())
	flag.Parse()

	overwriteFlag(port, defaultPort, "MUXWIKI_PORT")

	val, dataEnv := os.LookupEnv("MUXWIKI_DATADIR")
	if *dataDir == "" && dataEnv {
		*dataDir = val
	}
	if *dataDir != "" && !path.IsAbs(*dataDir) {
		log.Fatalln("You specified a relative data dir path, but it must be absolute")
	}
	if *dataDir == "" {
		log.Fatalln("You must specify an absolute path to a data directory to save everything to")
	}

	log.Println("Using data directory:", *dataDir)
	log.Println("Will run on port", *port)

	meta := meta.NewMeta(meta.Config{
		DataDir:      *dataDir,
		EnableMedia:  true,
		EnablePastes: true,
		EnableWiki:   true,
	})

	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.Recoverer)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Mount("/", meta.Router())

	log.Println("Initialization finished")
	err := http.ListenAndServe(":"+*port, r)
	if err != nil {
		log.Fatalln(err)
	}
}
