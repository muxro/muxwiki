let 
    pkgs = import <nixpkgs> {};
in
pkgs.buildGoPackage {
    name = "muxwiki";
    goPackagePath = "gitlab.com/muxro/muxwiki";
    src = ./.;
}