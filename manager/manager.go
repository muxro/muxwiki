// Package manager supplies the other components with a simple data manager and an interface to define your own
package manager

import (
	"io"
	"io/fs"
	"io/ioutil"
	"math/rand"
	"os"
	"path"
)

var letterRunes = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

// Manager defines a simple interface with a data manager
type Manager interface {
	SaveDataName(mode, name string, data io.Reader) error
	// SaveDataID writes the specified data to a new file on the specifed mode with an unique ID
	// (which is returned along with an accompanying error by the function)
	SaveDataID(mode string, data io.Reader) (string, error)
	// GetData returns the data from a file with a specified name (either ID or actual name) in the specified mode
	// (along with an error)
	GetData(mode string, name string) ([]byte, error)
	GetFile(mode, name string) (io.ReadSeekCloser, fs.FileInfo, error)
	// IDExists says if a specific id exists for the corresponding mode
	IDExists(mode string, id string) bool
}

// manager is a minimal implementation that simply writes everything to disk
type manager struct {
	dir   string
	idLen int
}

func (m manager) SaveDataName(mode, name string, data io.Reader) error {
	if err := os.MkdirAll(path.Join(m.dir, mode), 0775); err != nil {
		return err
	}
	f, err := os.OpenFile(path.Join(m.dir, mode, name), os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	_, err = io.Copy(f, data)
	if err1 := f.Close(); err1 != nil && err == nil {
		err = err1
	}
	return err
}

func (m manager) SaveDataID(mode string, data io.Reader) (string, error) {
	id := m.generateValidID(mode)
	return id, m.SaveDataName(mode, id, data)
}

func (m manager) GetData(mode string, id string) ([]byte, error) {
	// prevent nasty stuff
	id = path.Base(path.Clean(id))
	return ioutil.ReadFile(path.Join(m.dir, mode, id))
}

func (m manager) GetFile(mode, id string) (io.ReadSeekCloser, fs.FileInfo, error) {
	id = path.Base(path.Clean(id))
	p := path.Join(m.dir, mode, id)
	f, err := os.Open(p)
	if err != nil {
		return nil, nil, err
	}

	stat, err := f.Stat()
	if err != nil {
		return nil, nil, err
	}

	return f, stat, nil
}

func (m manager) IDExists(mediaType string, name string) bool {
	name = path.Base(path.Clean(name))
	if name == "" || name == "." {
		return false
	}
	_, err := os.Stat(path.Join(m.dir, mediaType, name))
	return !os.IsNotExist(err)
}

// generateValidID generates a valid id for the corresponding media type
func (m manager) generateValidID(mediaType string) string {
	var retID = m.randomID(m.idLen)
	for m.IDExists(mediaType, retID) {
		retID = m.randomID(m.idLen)
	}
	return retID
}

// randomID generates a random ID
func (m manager) randomID(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

// NewManager returns a simple implementation of the Manager interface that writes everything to disk in the specified directory
func NewManager(dir string, IDLen int) Manager {
	return &manager{dir: dir, idLen: IDLen}
}
