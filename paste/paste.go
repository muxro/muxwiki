package paste

import (
	"fmt"
	"io"
	"net/http"
	"net/url"

	"github.com/go-chi/chi/v5"
	"gitlab.com/muxro/muxwiki/manager"
)

// Paste is the wrapper struct for the package
type Paste struct {
	manager manager.Manager

	mode    string
	urlPath string
}

// HandlePasteUpload saves the paste to the disc
func (p *Paste) HandlePasteUpload(fileData io.Reader, fileName string, w http.ResponseWriter, r *http.Request) {
	id, err := p.manager.SaveDataID(p.mode, fileData)
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
	fmt.Fprintf(w, "%s/%s/%s/%s\n", GetHostname(r), p.urlPath, id, url.PathEscape(fileName))
}

// RenderPaste sends the paste data
func (p *Paste) RenderPaste(w http.ResponseWriter, r *http.Request) {
	pasteName := chi.URLParam(r, "paste")
	if !p.manager.IDExists(p.mode, pasteName) {
		http.Error(w, "Invalid ID", http.StatusBadRequest)
		return
	}
	name := pasteName
	if chi.URLParam(r, "filename") != "" {
		name = chi.URLParam(r, "filename")
	}
	file, stat, err := p.manager.GetFile(p.mode, pasteName)
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
	defer file.Close()
	http.ServeContent(w, r, name, stat.ModTime(), file)
}

// Router returns a router for the Paste instance
func (p *Paste) Router() chi.Router {
	r := chi.NewRouter()
	r.Route("/{paste}", func(r chi.Router) {
		r.Get("/", p.RenderPaste)
		r.Get("/{filename}", p.RenderPaste)
	})
	return r
}

// NewPaste returns a new Paste instance
func NewPaste(manager manager.Manager, mode string, path string) Paste {
	return Paste{manager, mode, path}
}

// GetHostname returns the hostname (in the form `http://example.com/`) from a request
func GetHostname(r *http.Request) string {
	scheme := "https"
	//if r.TLS != nil {
	//	scheme = "https"
	//}
	return fmt.Sprintf("%s://%s", scheme, r.Host)
}
